// sidebar
var open = document.querySelector('.icone-bars');
var sidebar = document.querySelector('.menu');

open.addEventListener('click', function(e){
    e.stopPropagation();
    sidebar.classList.add('active')
})

window.addEventListener('click', function(){
    sidebar.classList.remove('active')
})

// owl carousel commentaire
$('#comment').owlCarousel({
    loop:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:6000,
    autoplayHoverPause:true,
    nav:true,
    navText: [
        "<i class='fa fa-long-arrow-left'></i>",
        "<i class='fa fa-long-arrow-right'></i>"
      ],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:2
        }
    }
})

// owl carousel commentaire
$('#team').owlCarousel({
    loop:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:6000,
    autoplayHoverPause:true,
    nav:true,
    navText: [
        "<i class='fa fa-long-arrow-left'></i>",
        "<i class='fa fa-long-arrow-right'></i>"
      ],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    }
})